FROM rust:latest

RUN USER=root cargo new --bin executor

WORKDIR ./executor

COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
