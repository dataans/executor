FROM eu.gcr.io/dataans/executor_builder:5 as builder

COPY ./Cargo.toml ./Cargo.toml
RUN rm src/*.rs

ADD src ./src
ADD deploy ./deploy

RUN rm ./target/release/deps/executor*
RUN cargo build --release

FROM frolvlad/alpine-glibc:glibc-2.29
ARG APP=/usr/src/app

EXPOSE 8000

COPY --from=builder /executor/target/release/executor ./executor

CMD ["./executor"]
