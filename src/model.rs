use std::convert::TryFrom;

use deadpool_postgres::tokio_postgres::Row;
use time::OffsetDateTime;
use uuid::Uuid;

pub struct Sample {
    pub id: Uuid,
    pub query: String,
    pub schema: String,
    pub last_execution: OffsetDateTime,
    pub creator_id: Uuid,
}

impl TryFrom<Row> for Sample {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            query: row.try_get(1)?,
            schema: row.try_get(2)?,
            last_execution: row.try_get(3)?,
            creator_id: row.try_get(4)?,
        })
    }
}
