use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

use crate::model::Sample;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ExecuteRequest {
    pub id: Option<Uuid>,
    pub query: Option<String>,
    pub schema: Option<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SampleUpdate {
    pub id: Uuid,
    pub query: Option<String>,
    pub schema: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SampleResponse {
    pub id: Uuid,
    pub query: String,
    pub schema: String,
    #[serde(with = "rfc3339")]
    pub last_execution: OffsetDateTime,
    pub creator_id: Uuid,
}

impl From<Sample> for SampleResponse {
    fn from(sample: Sample) -> Self {
        let Sample {
            id,
            query,
            schema,
            last_execution,
            creator_id,
        } = sample;
        SampleResponse {
            id,
            query,
            schema,
            last_execution,
            creator_id,
        }
    }
}

impl Responder for SampleResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}
