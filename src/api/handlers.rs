use actix_web::{
    delete, get, http::StatusCode, post, put, web, HttpRequest, HttpResponse, Responder,
};
use serde::Deserialize;
use uuid::Uuid;

use super::types::ExecuteRequest;
use crate::{
    api::types::SampleUpdate, app::AppData, services::executor::ExecutorError,
    services::sample::SampleError,
};

pub const AUTH_COOKIE_NAME: &str = "SessionId";

#[derive(Deserialize)]
pub struct Id {
    pub id: Uuid,
}

pub async fn default_route() -> impl Responder {
    HttpResponse::Ok().status(StatusCode::NOT_FOUND).body(())
}

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}

#[get("/api/v1/execute/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("executor ok.")
}

#[post("/api/v1/sample")]
pub async fn create_sample(
    execute_request: web::Json<ExecuteRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| SampleError::NotAuthorized("is not authorized".into()))?;

    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.sample_service
        .create(&execute_request, &session.user_id)
        .await
}

#[put("/api/v1/sample")]
pub async fn update_sample(
    data: web::Json<SampleUpdate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| SampleError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.sample_service
        .update(data.into_inner(), &session.user_id)
        .await
}

#[get("/api/v1/sample/{id}")]
pub async fn get_by_id(params: web::Path<Id>, app: web::Data<AppData>) -> impl Responder {
    app.sample_service.get_by_id(&params.id).await
}

#[delete("/api/v1/sample/{id}")]
pub async fn delete_by_id(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| SampleError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.sample_service
        .delete(&params.id, &session.user_id)
        .await
}

#[post("/api/v1/execute")]
pub async fn execute(
    execute_request: web::Json<ExecuteRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| ExecutorError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.executor_service
        .execute(&execute_request, &session.user_id)
        .await
}
