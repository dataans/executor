use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use async_mutex::Mutex;
use deadpool_postgres::{tokio_postgres::NoTls, Runtime};
use session_manager::SessionService;

use crate::{
    api::handlers::{
        create_sample, default_route, delete_by_id, execute, get_by_id, health, root_health,
        update_sample,
    },
    config::{bind_address, check_env_vars, pool_config},
    db::SampleRepository,
    logging::setup_logger,
    services::{
        executor::{k8s::K8s, ExecutorService},
        sample::SampleService,
    },
    ws::start_ws,
};

pub struct AppData {
    pub session_service: Mutex<SessionService>,
    pub executor_service: Arc<ExecutorService>,
    pub sample_service: Arc<SampleService>,
    pub k8s: Arc<K8s>,
}

impl AppData {
    pub fn default_with_k8s(k8s: K8s) -> Self {
        let pool = Arc::new(Mutex::new(
            pool_config()
                .create_pool(Some(Runtime::Tokio1), NoTls)
                .unwrap(),
        ));
        let k8s = Arc::new(k8s);
        let sample_service = Arc::new(SampleService::new(SampleRepository::new(pool), k8s.clone()));

        Self {
            session_service: Mutex::new(SessionService::new_from_env()),
            executor_service: Arc::new(ExecutorService::new(sample_service.clone(), k8s.clone())),
            sample_service,
            k8s,
        }
    }
}

#[allow(deprecated)]
pub async fn start_app() -> std::io::Result<()> {
    check_env_vars();
    setup_logger();

    let k8s = crate::services::executor::k8s::K8s::try_default()
        .await
        .unwrap();

    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .default_service(web::route().to(default_route))
            .data(AppData::default_with_k8s(k8s.clone()))
            .service(root_health)
            .service(health)
            .service(execute)
            .service(create_sample)
            .service(get_by_id)
            .service(delete_by_id)
            .service(update_sample)
            .service(web::resource("/api/v1/ws").route(web::get().to(start_ws)))
    })
    .bind(bind_address())?
    .workers(4)
    .run()
    .await
}
