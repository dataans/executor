use std::{convert::TryFrom, sync::Arc};

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use thiserror::Error;
use uuid::Uuid;

use crate::model::Sample;

const ADD_CODE_SAMPLE: &str =
    "insert into samples (id, query, schema, creator_id) values ($1, $2, $3, $4)";
const UPDATE_CODE_SAMPLE: &str = "update samples set query=$2, schema=$3 where id=$1";
const FIND_BY_ID: &str = "select * from samples where id = $1";
const DELETE_BY_ID: &str = "delete from samples where id = $1";

#[derive(Error, Debug)]
pub enum DbError {
    #[error("Database unavailable")]
    Unavailable,
    #[error("Query error: {0:?}")]
    QueryError(#[from] deadpool_postgres::tokio_postgres::Error),
    #[error("Db connection error: {0:?}")]
    DbConnectionError(#[from] PoolError),
}

pub struct SampleRepository {
    pool: Arc<Mutex<Pool>>,
}

impl SampleRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        SampleRepository { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn save(&self, data: &Sample) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_CODE_SAMPLE).await?;

        client
            .query(
                &smt,
                &[&data.id, &data.query, &data.schema, &data.creator_id],
            )
            .await?;

        Ok(())
    }

    pub async fn update(&self, data: &Sample) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_CODE_SAMPLE).await?;

        client
            .query(&smt, &[&data.id, &data.query, &data.schema])
            .await?;

        Ok(())
    }

    pub async fn find_by_id(&self, id: &Uuid) -> Result<Option<Sample>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[id]).await?.into_iter().next() {
            Some(row) => Ok(Some(Sample::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn delete_by_id(&self, id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_BY_ID).await?;

        client.execute(&smt, &[id]).await?;

        Ok(())
    }
}
