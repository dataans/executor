use std::env;

pub fn bind_address() -> String {
    format!(
        "0.0.0.0:{}",
        env::var("PORT").expect("Missing PORT environment variable")
    )
}

pub fn check_env_vars() {
    let cipher_key = env::var(session_manager::CIPHER_KEY_ENV).unwrap_or_else(|_| {
        panic!(
            "Missing {} environment variable",
            session_manager::CIPHER_KEY_ENV
        )
    });
    // cipher_key must be 32 bytes len. In hex it equals to 64 chars
    assert_eq!(cipher_key.len(), 64);
    // check if cipher key is valid hex bytes
    hex::decode(cipher_key).unwrap();

    env::var(session_manager::REDIS_URL_ENV).unwrap_or_else(|_| {
        panic!(
            "Missing {} environment variable",
            session_manager::REDIS_URL_ENV
        )
    });
}

pub fn pool_config() -> deadpool_postgres::Config {
    let mut config = deadpool_postgres::Config::new();
    config.dbname = Some(env::var("DB_NAME").expect("Missing DB_NAME environment variable"));
    config.user = Some(env::var("DB_USER").expect("Missing DB_USER environment variable"));
    config.password =
        Some(env::var("DB_PASSWORD").expect("Missing DB_PASSWORD environment variable"));
    config.host = Some("localhost".to_owned());
    config
}
