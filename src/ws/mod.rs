mod handlers;
mod types;
mod utils;

use std::{
    sync::Arc,
    time::{Duration, Instant},
};

use actix::{Actor, ActorContext, AsyncContext, Handler, Message, StreamHandler};
use actix_web::{web, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use uuid::Uuid;

use crate::{
    api::handlers::AUTH_COOKIE_NAME,
    app::AppData,
    services::{
        executor::k8s::K8s,
        sample::{SampleError, SampleService},
    },
    try_execute,
    ws::handlers::execute_handler,
};

use self::types::{ErrorResponse, StringResponse};

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

pub async fn start_ws(
    app: web::Data<AppData>,
    req: HttpRequest,
    stream: web::Payload,
) -> Result<HttpResponse, Error> {
    println!("in start ws");
    let session_cookie = req.cookie(AUTH_COOKIE_NAME).ok_or_else(|| {
        println!("ws: reject: no cookies");
        Error::from(SampleError::NotAuthorized("is not authorized".into()))
    })?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await
        .map_err(|err| {
            println!("ws: session error: {:?}", err);
            Error::from(SampleError::from(err))
        })?;

    ws::start(
        WsExecutor::new(app.sample_service.clone(), app.k8s.clone(), session.user_id),
        &req,
        stream,
    )
}

pub struct WsExecutor {
    sample_service: Arc<SampleService>,
    k8s: Arc<K8s>,
    hb: Instant,
    user_id: Uuid,
}

impl WsExecutor {
    pub fn new(sample_service: Arc<SampleService>, k8s: Arc<K8s>, user_id: Uuid) -> Self {
        Self {
            hb: Instant::now(),
            sample_service,
            k8s,
            user_id,
        }
    }

    fn hb(&self, ctx: &mut <Self as Actor>::Context) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                ctx.stop();
                return;
            }

            ctx.ping(b"");
        });
    }
}

impl Actor for WsExecutor {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::info!("new actor started");
        println!("new actor started");
        self.hb(ctx);
    }
}

impl Message for StringResponse {
    type Result = ();
}

impl Handler<StringResponse> for WsExecutor {
    type Result = ();

    fn handle(&mut self, msg: StringResponse, ctx: &mut Self::Context) {
        ctx.text(msg.0);
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsExecutor {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match msg {
            Ok(ws::Message::Ping(msg)) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            }
            Ok(ws::Message::Pong(_)) => {
                self.hb = Instant::now();
            }
            Ok(ws::Message::Text(text)) => {
                let text = text.to_string();
                let recipient = ctx.address().recipient();

                let execute_request = try_execute! { serde_json::from_str(&text), recipient, "" };

                let fut = actix::fut::wrap_future::<_, Self>(execute_handler(
                    execute_request,
                    self.user_id,
                    recipient,
                    self.sample_service.clone(),
                    self.k8s.clone(),
                ));
                ctx.spawn(fut);
            }
            Ok(ws::Message::Close(reason)) => {
                ctx.close(reason);
                ctx.stop();
            }
            _ => ctx.stop(),
        }
    }
}
