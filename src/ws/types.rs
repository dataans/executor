use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct OkResponse {
    pub message: String,
    pub data: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ErrorResponse {
    pub message: String,
    pub error: String,
}

pub struct StringResponse(pub String);
