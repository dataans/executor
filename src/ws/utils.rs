#[macro_export]
macro_rules! try_execute {
    ($x:expr, $r:expr, $m:expr) => {{
        match $x {
            Ok(value) => value,
            Err(err) => {
                $r.do_send(StringResponse(
                    serde_json::to_string(&ErrorResponse {
                        message: $m.into(),
                        error: err.to_string(),
                    })
                    .unwrap(),
                ));
                return;
            }
        }
    }};
}

#[macro_export]
macro_rules! ws_simple_message {
    ($m:expr) => {{
        StringResponse(
            serde_json::to_string(&OkResponse {
                message: $m,
                data: None,
            })
            .unwrap(),
        )
    }};
}
