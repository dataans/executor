use std::sync::Arc;

use actix::Recipient;
use sqlparser::dialect::PostgreSqlDialect;
use time::OffsetDateTime;
use uuid::Uuid;

use super::types::{ErrorResponse, StringResponse};
use crate::{
    api::types::{ExecuteRequest, SampleResponse},
    model::Sample,
    services::{
        executor::{k8s::K8s, postgres::PostgresExecutor, utils::split_sql_code, DbExecutor},
        sample::SampleService,
    },
    try_execute,
    ws::types::OkResponse,
    ws_simple_message,
};

pub async fn execute_handler(
    execute_request: ExecuteRequest,
    user_id: Uuid,
    recipient: Recipient<StringResponse>,
    sample_service: Arc<SampleService>,
    k8s: Arc<K8s>,
) {
    let ExecuteRequest { id, query, schema } = execute_request;

    let sample = if let Some(sample_id) = id.as_ref() {
        let mut sample = try_execute! {sample_service.get_by_id(sample_id).await, recipient, ""};

        if schema.is_some() {
            sample.schema = schema.clone().unwrap();
        }
        if query.is_some() {
            sample.query = query.clone().unwrap();
        }
        let SampleResponse {
            id,
            query,
            schema,
            last_execution,
            creator_id,
        } = sample;
        Sample {
            id,
            query,
            schema,
            last_execution,
            creator_id,
        }
    } else {
        Sample {
            id: id.unwrap_or_else(Uuid::new_v4),
            query: query.clone().unwrap_or_default(),
            schema: schema.clone().unwrap_or_default(),
            last_execution: OffsetDateTime::now_utc(),
            creator_id: user_id,
        }
    };
    // create a new sample if it not exist, update if exist
    sample_service.save(&sample).await.unwrap();

    // create a new pod if it not exist
    let pod_name = format!("sample-{}", sample.id);
    let pod_ip = if let Some(pod) = try_execute! { k8s.pod_by_name(&pod_name).await, recipient, "" }
    {
        pod.status.unwrap().pod_ip.unwrap()
    } else {
        recipient.do_send(ws_simple_message!("Start provisioning resources...".into()));

        let id = sample.id.to_string();

        try_execute! { k8s.provide_sample_storage(&id).await, recipient, "Can not attach sample storage :(" };
        recipient.do_send(ws_simple_message!(
            "Sample storage attached. Starting database...".into()
        ));

        try_execute! { k8s.provide_sample_pod(&id).await, recipient, "Can not start sample database :(" };
        try_execute! { k8s.wait_until_pod_is_running(&pod_name).await, recipient, "Can not start sample database :(" };

        try_execute! { k8s.pod_by_name(&pod_name).await, recipient, "" }
            .unwrap()
            .status
            .unwrap()
            .pod_ip
            .unwrap()
    };

    recipient.do_send(ws_simple_message!(
        "All needed database resources provided".into()
    ));

    // execute sql code and return the data
    let mut executor = PostgresExecutor::new(
        "postgres".into(),
        "postgres".into(),
        "postgres".into(),
        pod_ip,
    );

    let is_empty = schema.is_none() && query.is_none();

    if schema.is_some() || is_empty {
        try_execute! { executor.schema(&sample.schema).await, recipient, "Error in schema execution" };
    }
    recipient.do_send(ws_simple_message!(
        "Schema updated! Started querying...".into()
    ));

    if query.is_some() || is_empty {
        let queries = split_sql_code(&sample.query, &PostgreSqlDialect {});
        let result =
            try_execute! { executor.query(&queries).await, recipient, "Error in query execution" };
        let response = OkResponse {
            message: "".into(),
            data: Some(serde_json::to_string(&result).unwrap()),
        };
        recipient.do_send(StringResponse(serde_json::to_string(&response).unwrap()));
    }
    recipient.do_send(ws_simple_message!("Done!".into()));
}
