#[actix_web::main]
async fn main() -> std::io::Result<()> {
    executor::app::start_app().await
}
