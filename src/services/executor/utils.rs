use sqlparser::{
    dialect::Dialect,
    tokenizer::{Token, TokenWithLocation, Tokenizer, Whitespace},
};

fn search_for_comment(tokens: &[TokenWithLocation], from_index: usize) -> Option<String> {
    for i in from_index..tokens.len() {
        match &tokens.get(i).unwrap().token {
            Token::Whitespace(Whitespace::SingleLineComment { comment, prefix: _ }) => {
                return Some(comment.trim().into())
            }
            Token::Whitespace(_) => {}
            _ => return None,
        }
    }

    None
}

// (String, &'a str) -> (query name, query code)
// query name - content of the comment before the query
pub fn split_sql_code<'a>(code: &'a str, dialect: &'a dyn Dialect) -> Vec<(String, &'a str)> {
    let mut tokenizer = Tokenizer::new(dialect, code);

    let tokens = match tokenizer.tokenize_with_location() {
        Ok(tokens) => tokens,
        Err(_) => return vec![("query result".into(), code)],
    };

    let mut queries = Vec::new();

    let mut prev_position = 1;
    let mut prev_comment = search_for_comment(&tokens, 0).unwrap_or_else(|| "query result".into());
    for i in 0..tokens.len() {
        let token = tokens.get(i).unwrap();
        if token.token == Token::SemiColon {
            let position = token.location.absolute_position as usize;

            let query = code[(prev_position - 1)..position].trim();
            if !query.is_empty() {
                let comment =
                    search_for_comment(&tokens, i + 1).unwrap_or_else(|| "query result".into());

                queries.push((prev_comment, query));
                prev_comment = comment;
            }

            prev_position = position + 1;
        }
    }

    queries
}

#[cfg(test)]
mod tests {
    use sqlparser::dialect::GenericDialect;

    use super::split_sql_code;

    #[test]
    fn simple_split() {
        println!(
            "{:?}",
            split_sql_code(
                "-- my bad query\nselect users.id, name from users right join unnest('{1, 2, 2, 1, 3}'::integer[]) as ids on ids = users.id;\n-- select all users\nselect * from users;",
                &GenericDialect::default()
            )
        );
    }
}
