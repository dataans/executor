pub mod k8s;
pub mod postgres;
pub mod utils;

use std::sync::Arc;

use actix_web::{http::StatusCode, ResponseError};
use async_trait::async_trait;
use k8s::{K8s, K8sError};
use serde::{Deserialize, Serialize};
use serde_json::value::Value;
use session_manager::SessionError;
use sqlparser::dialect::PostgreSqlDialect;
use thiserror::Error;
use time::OffsetDateTime;
use uuid::Uuid;

use crate::{
    api::types::{ExecuteRequest, SampleResponse},
    model::Sample,
    services::sample::{SampleError, SampleService},
};

use self::utils::split_sql_code;

#[derive(Error, Debug)]
pub enum ExecutorError {
    #[error("{0:?}")]
    Sample(#[from] SampleError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("K8s error: {0:?}")]
    K8sError(#[from] K8sError),
    #[error("{0:?}")]
    PoolError(#[from] deadpool_postgres::PoolError),
    #[error("{0:?}")]
    QueryError(#[from] deadpool_postgres::tokio_postgres::Error),
    #[error("Unsupported type: {0}")]
    UnsupportedType(String),
}

impl From<SessionError> for ExecutorError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => ExecutorError::NotAuthorized(error),
            SessionError::Crypto(error) => ExecutorError::Internal(error),
            SessionError::RedisConnection(error) => ExecutorError::Internal(format!("{}", error)),
        }
    }
}

impl ResponseError for ExecutorError {
    fn status_code(&self) -> StatusCode {
        match self {
            ExecutorError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            ExecutorError::Sample(e) => e.status_code(),
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct QueryResult {
    pub name: String,
    pub data: Value,
}

#[async_trait]
pub trait DbExecutor {
    async fn schema(&mut self, code: &str) -> Result<(), ExecutorError>;
    async fn query(
        &mut self,
        queries: &[(String, &str)],
    ) -> Result<Vec<QueryResult>, ExecutorError>;
}

pub struct ExecutorService {
    sample_service: Arc<SampleService>,
    k8s: Arc<K8s>,
}

impl ExecutorService {
    pub fn new(sample_service: Arc<SampleService>, k8s: Arc<K8s>) -> Self {
        Self {
            sample_service,
            k8s,
        }
    }

    pub async fn execute(
        &self,
        execute_request: &ExecuteRequest,
        user_id: &Uuid,
    ) -> Result<String, ExecutorError> {
        let ExecuteRequest { id, query, schema } = execute_request.clone();

        let sample = if let Some(sample_id) = id.as_ref() {
            let mut sample = self.sample_service.get_by_id(sample_id).await?;

            if schema.is_some() {
                sample.schema = schema.unwrap();
            }
            if query.is_some() {
                sample.query = query.unwrap();
            }
            let SampleResponse {
                id,
                query,
                schema,
                last_execution,
                creator_id,
            } = sample;
            Sample {
                id,
                query,
                schema,
                last_execution,
                creator_id,
            }
        } else {
            Sample {
                id: id.unwrap_or_else(Uuid::new_v4),
                query: query.unwrap_or_default(),
                schema: schema.unwrap_or_default(),
                last_execution: OffsetDateTime::now_utc(),
                creator_id: *user_id,
            }
        };
        // create a new sample if it not exist, update if exist
        self.sample_service.save(&sample).await?;

        // create a new pod if it not exist
        let pod_name = format!("sample-{}", sample.id);
        let pod_ip = if let Some(pod) = self.k8s.pod_by_name(&pod_name).await? {
            pod.status.unwrap().pod_ip.unwrap()
        } else {
            self.k8s
                .create_sample_database(&sample.id.to_string())
                .await?;
            self.k8s.wait_until_pod_is_running(&pod_name).await?;
            self.k8s
                .pod_by_name(&pod_name)
                .await?
                .unwrap()
                .status
                .unwrap()
                .pod_ip
                .unwrap()
        };

        // execute sql code and return the data
        let mut executor = postgres::PostgresExecutor::new(
            "postgres".into(),
            "postgres".into(),
            "postgres".into(),
            pod_ip,
        );
        if let Some(schema) = execute_request.schema.as_ref() {
            executor.schema(schema).await?;
        }

        if let Some(query) = execute_request.query.as_ref() {
            let queries = split_sql_code(query, &PostgreSqlDialect {});

            Ok(serde_json::to_string(&executor.query(&queries).await?).unwrap())
        } else {
            Ok("".into())
        }
    }
}

#[cfg(test)]
mod tests {
    use k8s_openapi::api::core::v1::{PersistentVolumeClaim, Pod};
    use kube::{
        api::{Api, ListParams, PostParams},
        runtime::wait::{await_condition, conditions::is_pod_running},
        Client,
    };
    use uuid::Uuid;

    #[tokio::test]
    async fn query_pods_list() {
        let client = Client::try_default().await.unwrap();

        for pod in Api::<Pod>::default_namespaced(client)
            .list(&ListParams::default())
            .await
            .unwrap()
            .items
        {
            println!("{:?}", pod.status.unwrap().pod_ip);
        }
    }

    #[tokio::test]
    async fn create_sample_pod() {
        let id = Uuid::new_v4().to_string();
        println!("new pod name: {}", id);

        let pod: Pod = serde_json::from_str(
            &include_str!("../../../deploy/sample_pod_example.json").replace("$id$", &id),
        )
        .unwrap();

        let client = Client::try_default().await.unwrap();
        let pods: Api<Pod> = Api::default_namespaced(client);
        let post_params = PostParams::default();

        pods.create(&post_params, &pod).await.unwrap();
    }

    #[tokio::test]
    async fn create_sample_pod_with_claim() {
        let id = Uuid::new_v4().to_string();
        println!("new id: {}", id);

        let post_params = PostParams::default();

        let pvc: PersistentVolumeClaim = serde_json::from_str(
            &include_str!("../../../deploy/sample_pvc_example.json").replace("$id$", &id),
        )
        .unwrap();
        let pvcs: Api<PersistentVolumeClaim> =
            Api::default_namespaced(Client::try_default().await.unwrap());

        pvcs.create(&post_params, &pvc).await.unwrap();

        let pod: Pod = serde_json::from_str(
            &include_str!("../../../deploy/sample_pod_example.json").replace("$id$", &id),
        )
        .unwrap();
        let pods: Api<Pod> = Api::default_namespaced(Client::try_default().await.unwrap());

        pods.create(&post_params, &pod).await.unwrap();
    }

    #[tokio::test]
    async fn create_sample_pod_with_claim_and_await() {
        let id = Uuid::new_v4().to_string();
        println!("new id: {}", id);

        let post_params = PostParams::default();

        let pvc: PersistentVolumeClaim = serde_json::from_str(
            &include_str!("../../../deploy/sample_pvc_example.json").replace("$id$", &id),
        )
        .unwrap();
        let pvcs: Api<PersistentVolumeClaim> =
            Api::default_namespaced(Client::try_default().await.unwrap());

        pvcs.create(&post_params, &pvc).await.unwrap();

        let pod: Pod = serde_json::from_str(
            &include_str!("../../../deploy/sample_pod_example.json").replace("$id$", &id),
        )
        .unwrap();
        let pods: Api<Pod> = Api::default_namespaced(Client::try_default().await.unwrap());

        pods.create(&post_params, &pod).await.unwrap();

        let pod_name = format!("sample-{}", id);

        let establish = await_condition(pods.clone(), &pod_name, is_pod_running());
        let _ = tokio::time::timeout(std::time::Duration::from_secs(45), establish)
            .await
            .unwrap();
        let sample_pod = pods.get(&pod_name).await.unwrap();
        println!("{:?}", sample_pod);
    }
}
