use k8s_openapi::api::core::v1::{PersistentVolumeClaim, Pod};
use kube::{
    api::{Api, DeleteParams, ListParams, PostParams},
    runtime::wait::{await_condition, conditions::is_pod_running},
    Client,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum K8sError {
    #[error("Kube client error: {0:?}")]
    KubeClient(#[from] kube::Error),
    #[error("Timeout when provisioning resources/command execution")]
    Timeout,
}

#[derive(Clone)]
pub struct K8s {
    default_pods: Api<Pod>,
    default_claims: Api<PersistentVolumeClaim>,
}

impl K8s {
    pub async fn try_default() -> Result<Self, K8sError> {
        Ok(Self {
            default_pods: Api::default_namespaced(Client::try_default().await?),
            default_claims: Api::default_namespaced(Client::try_default().await?),
        })
    }

    pub async fn pod_by_name(&self, name: &str) -> Result<Option<Pod>, K8sError> {
        Ok(self
            .default_pods
            .list(&ListParams::default())
            .await?
            .items
            .into_iter()
            .find(|pod| pod.metadata.name.as_ref() == Some(&name.into())))
    }

    pub async fn claim_by_name(
        &self,
        name: &str,
    ) -> Result<Option<PersistentVolumeClaim>, K8sError> {
        Ok(self
            .default_claims
            .list(&ListParams::default())
            .await?
            .items
            .into_iter()
            .find(|claim| claim.metadata.name.as_ref() == Some(&name.into())))
    }

    pub async fn provide_sample_storage(&self, id: &str) -> Result<(), K8sError> {
        let post_params = PostParams::default();

        if self
            .claim_by_name(&format!("claim-{}", id))
            .await?
            .is_none()
        {
            let pvc: PersistentVolumeClaim = serde_json::from_str(
                &include_str!("../../../deploy/sample_pvc_example.json").replace("$id$", id),
            )
            .unwrap();

            self.default_claims.create(&post_params, &pvc).await?;
        }

        Ok(())
    }

    pub async fn provide_sample_pod(&self, id: &str) -> Result<(), K8sError> {
        let post_params = PostParams::default();

        let pod: Pod = serde_json::from_str(
            &include_str!("../../../deploy/sample_pod_example.json").replace("$id$", id),
        )
        .unwrap();

        self.default_pods.create(&post_params, &pod).await?;

        Ok(())
    }

    pub async fn create_sample_database(&self, id: &str) -> Result<(), K8sError> {
        let post_params = PostParams::default();

        if self
            .claim_by_name(&format!("claim-{}", id))
            .await?
            .is_none()
        {
            let pvc: PersistentVolumeClaim = serde_json::from_str(
                &include_str!("../../../deploy/sample_pvc_example.json").replace("$id$", id),
            )
            .unwrap();

            self.default_claims.create(&post_params, &pvc).await?;
        }

        let pod: Pod = serde_json::from_str(
            &include_str!("../../../deploy/sample_pod_example.json").replace("$id$", id),
        )
        .unwrap();

        self.default_pods.create(&post_params, &pod).await?;

        Ok(())
    }

    // deleted sample pod and attached pvc
    pub async fn delete_sample_database(&self, id: &str) -> Result<(), K8sError> {
        let claim_name = format!("claim-{}", id);
        let pod_name = format!("sample-{}", id);
        let delete_params = DeleteParams::default();

        if self.claim_by_name(&claim_name).await?.is_some() {
            self.default_claims
                .delete(&claim_name, &delete_params)
                .await?;
        }

        if self.pod_by_name(&pod_name).await?.is_some() {
            self.default_pods.delete(&pod_name, &delete_params).await?;
        }

        Ok(())
    }

    pub async fn wait_until_pod_is_running1(&self, pod_name: &str) -> Result<(), K8sError> {
        let _ = actix::clock::timeout(
            std::time::Duration::from_secs(60),
            await_condition(self.default_pods.clone(), pod_name, is_pod_running()),
        )
        .await
        .map_err(|_| K8sError::Timeout)?;

        Ok(())
    }

    pub async fn wait_until_pod_is_running(&self, pod_name: &str) -> Result<(), K8sError> {
        let _ = actix::clock::timeout(
            std::time::Duration::from_secs(60),
            await_condition(self.default_pods.clone(), pod_name, is_pod_running()),
        )
        .await
        .map_err(|_| K8sError::Timeout)?;

        Ok(())
    }
}
