use async_trait::async_trait;
use deadpool_postgres::{
    tokio_postgres::{types::Type, NoTls, Row},
    Config, Pool, Runtime,
};
use serde::{Deserialize, Serialize};
use serde_json::{map::Map, value::Value, Number};
use uuid::Uuid;

use super::{DbExecutor, ExecutorError, QueryResult};

#[derive(Deserialize, Serialize)]
struct Cell {}

pub struct PostgresExecutor {
    pool: Pool,
}

impl PostgresExecutor {
    pub fn new(user: String, password: String, db_name: String, host: String) -> Self {
        let mut config = Config::new();
        config.dbname = Some(db_name);
        config.user = Some(user);
        config.password = Some(password);
        config.host = Some(host);

        Self {
            pool: config.create_pool(Some(Runtime::Tokio1), NoTls).unwrap(),
        }
    }
}

#[async_trait]
impl DbExecutor for PostgresExecutor {
    async fn schema(&mut self, code: &str) -> Result<(), ExecutorError> {
        self.pool.get().await?.batch_execute(code).await?;

        Ok(())
    }

    async fn query(
        &mut self,
        queries: &[(String, &str)],
    ) -> Result<Vec<QueryResult>, ExecutorError> {
        let mut results = Vec::with_capacity(queries.len());

        for (query_name, query) in queries {
            let client = self.pool.get().await?;
            let smt = client.prepare(query).await?;

            let rows = client.query(&smt, &[]).await?;
            let mut objects = Vec::with_capacity(rows.len());

            for row in rows {
                let mut row_values = Vec::new();
                for column in row.columns().iter() {
                    let mut cell = Map::new();
                    cell.insert("name".into(), column.name().into());
                    cell.insert(
                        "value".into(),
                        row_value_to_json_value(column.name(), column.type_(), &row)?,
                    );

                    row_values.push(Value::Object(cell));
                }
                objects.push(Value::Array(row_values));
            }

            results.push(QueryResult {
                name: query_name.into(),
                data: Value::Array(objects),
            })
        }

        Ok(results)
    }
}

fn row_value_to_json_value(name: &str, t: &Type, row: &Row) -> Result<Value, ExecutorError> {
    match t {
        &Type::UUID => Ok(Value::String(row.get::<&str, Uuid>(name).to_string())),
        &Type::VARCHAR => Ok(Value::String(row.get::<&str, String>(name))),
        &Type::BOOL => Ok(Value::Bool(row.get::<&str, bool>(name))),
        &Type::INT2 => Ok(Value::Number(
            Number::from_f64(row.get::<&str, i16>(name) as f64).unwrap(),
        )),
        &Type::INT4 => Ok(Value::Number(
            Number::from_f64(row.get::<&str, i32>(name) as f64).unwrap(),
        )),
        &Type::FLOAT4 => Ok(Value::Number(
            Number::from_f64(row.get::<&str, f32>(name) as f64).unwrap(),
        )),
        &Type::FLOAT8 => Ok(Value::Number(
            Number::from_f64(row.get::<&str, f64>(name)).unwrap(),
        )),
        &Type::TEXT => Ok(Value::String(row.get::<&str, String>(name))),
        t => Err(ExecutorError::UnsupportedType(t.to_string())),
    }
}

#[cfg(test)]
mod tests {
    use deadpool_postgres::tokio_postgres::NoTls;
    use deadpool_postgres::Config;
    use deadpool_postgres::Runtime;
    use serde_json::map::Map;
    use serde_json::value::Value;

    use super::row_value_to_json_value;

    #[tokio::test]
    async fn test_rows_serialization() {
        let mut config = Config::new();
        config.dbname = Some("dataans".into());
        config.user = Some("postgres".into());
        config.password = Some("postgres".into());
        config.host = Some("localhost".into());

        let pool = config.create_pool(Some(Runtime::Tokio1), NoTls).unwrap();
        let client = pool.get().await.unwrap();
        let smt = client.prepare("select * from users").await.unwrap();

        let rows = client.query(&smt, &[]).await.unwrap();
        let json = Value::Array(
            rows.into_iter()
                .map(|row| {
                    Value::Object(row.columns().iter().fold(Map::new(), |mut m, e| {
                        m.insert(
                            e.name().into(),
                            row_value_to_json_value(e.name(), e.type_(), &row).unwrap(),
                        );
                        m
                    }))
                })
                .collect(),
        );
        println!("{:?}", json.to_string());
    }

    #[tokio::test]
    async fn test_statements_execution() {
        let mut config = Config::new();
        config.dbname = Some("dataans".into());
        config.user = Some("postgres".into());
        config.password = Some("postgres".into());
        config.host = Some("localhost".into());

        let pool = config.create_pool(Some(Runtime::Tokio1), NoTls).unwrap();
        let client = pool.get().await.unwrap();
        client.batch_execute("create table users2 (id varchar(10) primary key, name varchar(20) not null unique);insert into users2 values ('1', 'pasha'), ('2', 'misha')").await.unwrap();
    }
}
