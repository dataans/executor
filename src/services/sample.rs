use std::sync::Arc;

use actix_web::{http::StatusCode, ResponseError};
use session_manager::SessionError;
use thiserror::Error;
use time::OffsetDateTime;
use uuid::Uuid;

use crate::{
    api::{
        types::{ExecuteRequest, SampleResponse, SampleUpdate},
        EmptyResponse,
    },
    db::{DbError, SampleRepository},
    model::Sample,
};

use super::executor::k8s::{K8s, K8sError};

#[derive(Error, Debug)]
pub enum SampleError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("Sample with id {0:?} not found")]
    NotFound(Uuid),
    #[error("K8s error: {0:?}")]
    K8s(#[from] K8sError),
    #[error("Permission denied: {0}")]
    PermissionDenied(String),
}

impl From<SessionError> for SampleError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => SampleError::NotAuthorized(error),
            SessionError::Crypto(error) => SampleError::Internal(error),
            SessionError::RedisConnection(error) => SampleError::Internal(format!("{}", error)),
        }
    }
}

impl ResponseError for SampleError {
    fn status_code(&self) -> StatusCode {
        match self {
            SampleError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            SampleError::NotFound(_) => StatusCode::NOT_FOUND,
            SampleError::PermissionDenied(_) => StatusCode::FORBIDDEN,
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct SampleService {
    sample_repository: SampleRepository,
    k8s: Arc<K8s>,
}

impl SampleService {
    pub fn new(sample_repository: SampleRepository, k8s: Arc<K8s>) -> Self {
        Self {
            sample_repository,
            k8s,
        }
    }

    pub async fn create(
        &self,
        data: &ExecuteRequest,
        user_id: &Uuid,
    ) -> Result<SampleResponse, SampleError> {
        let ExecuteRequest { id, query, schema } = data.clone();
        let sample = Sample {
            id: id.unwrap_or_else(Uuid::new_v4),
            query: query.unwrap_or_default(),
            schema: schema.unwrap_or_default(),
            last_execution: OffsetDateTime::now_utc(),
            creator_id: *user_id,
        };

        self.sample_repository.save(&sample).await?;

        Ok(self
            .sample_repository
            .find_by_id(&sample.id)
            .await?
            .unwrap()
            .into())
    }

    pub async fn get_by_id(&self, id: &Uuid) -> Result<SampleResponse, SampleError> {
        Ok(self
            .sample_repository
            .find_by_id(id)
            .await?
            .ok_or(SampleError::NotFound(*id))?
            .into())
    }

    pub async fn save(&self, sample: &Sample) -> Result<(), SampleError> {
        match self.sample_repository.find_by_id(&sample.id).await? {
            Some(_) => self.sample_repository.update(sample).await,
            None => self.sample_repository.save(sample).await,
        }?;

        Ok(())
    }

    pub async fn delete(
        &self,
        sample_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<EmptyResponse, SampleError> {
        let sample = self
            .sample_repository
            .find_by_id(sample_id)
            .await?
            .ok_or(SampleError::NotFound(*sample_id))?;

        if sample.creator_id != *user_id {
            return Err(SampleError::PermissionDenied(
                "User can delete only their created samples".into(),
            ));
        }

        self.k8s
            .delete_sample_database(&sample_id.to_string())
            .await?;

        self.sample_repository.delete_by_id(sample_id).await?;

        Ok(EmptyResponse::new(StatusCode::NO_CONTENT))
    }

    pub async fn update(
        &self,
        data: SampleUpdate,
        user_id: &Uuid,
    ) -> Result<SampleResponse, SampleError> {
        let SampleUpdate { id, query, schema } = data;

        let mut sample = self
            .sample_repository
            .find_by_id(&id)
            .await?
            .ok_or(SampleError::NotFound(id))?;

        if sample.creator_id != *user_id {
            return Err(SampleError::PermissionDenied(
                "User can update only their created samples".into(),
            ));
        }

        if let Some(schema) = schema {
            sample.schema = schema;
        }

        if let Some(query) = query {
            sample.query = query;
        }

        self.sample_repository.update(&sample).await?;

        self.get_by_id(&id).await
    }
}
